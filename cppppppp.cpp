﻿#include <iostream>
using namespace std;

int main()
{
	int N;
	cin >> N; //это для ввода числа
	//четные
	std::cout << "Even: \n";
	for (int i = 0; i <= N; i++)
	{
		if (i % 2 == 0)
		{

			std::cout << i << "\n";
		}
	}
	//нечетные
	std::cout << "Odd: \n";
	for (int i = 0; i <= N; i++)
	{
		if (i % 2 == 1)
		{
			std::cout << i << "\n";
		}
	}
}